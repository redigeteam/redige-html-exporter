(function () {
    "use strict";

    function HTMLExporter() {
    }

    function getSectionTag(section) {
        if (section.type === 'hierarchical') {
            if (section.level === "0") {
                return 'p';
            } else {
                return 'h' + section.level;
            }
        } else if (section.type === 'olist' || section.type === 'ulist') {
            return 'li';
        } else if (section.type === 'image') {
            return 'img';
        }
    }

    function getListTag(type) {
        if (type === 'ulist') {
            return 'ul';
        } else if (type === 'olist') {
            return 'ol';
        }
    }

    function getDivisionOpenningTag(division) {
        if (division.type === 'blockquote') {
            return '<blockquote>';
        } else if (division.type === 'code') {
            return '<code><pre>';
        } else {
            return '<div>';
        }
    }

    function getDivisionClosingTag(division) {
        if (division.type === 'blockquote') {
            return '</blockquote>';
        } else if (division.type === 'code') {
            return '</pre></code>';
        } else {
            return '</div>';
        }
    }

    function getDecoratorClosingTag(decorator) {
        if (decorator.type === 'bold') {
            return '</b>';
        } else if (decorator.type === 'quote') {
            return '</cite>';
        } else if (decorator.type === 'link') {
            return '</a>';
        } else if (decorator.type === 'code') {
            return '</code>';
        } else {
            return '';
        }
    }

    function getDecoratorOpenningTag(decorator) {
        if (decorator.type === 'bold') {
            return '<b>';
        } else if (decorator.type === 'quote') {
            return '<cite>';
        } else if (decorator.type === 'link') {
            return '<a href="' + decorator.data + '">';
        } else if (decorator.type === 'code') {
            return '<code>';
        } else {
            return '';
        }
    }

    function convertSectionContent(section) {
        let content = "";
        let index = 0;
        for (; index < section.content.length; index++) {
            let currentChar = section.content.substring(index, index + 1);
            content += closeInlineTag(section, index);
            content += openInlineTag(section, index);
            content += htmlEncode(currentChar);
        }
        content += closeInlineTag(section, index);
        return content;
    }

    const escapeHTML = {
        '<': '&lt;',
        '>': '&gt;'
    };

    function htmlEncode(currentchar) {
        return escapeHTML[currentchar] || currentchar;
    }

    HTMLExporter.htmlEncode = htmlEncode;

    function closeInlineTag(section, offset) {
        let tagToClose = section.decorators.filter(function (decorator) {
            return offset === decorator.end;
        });
        tagToClose.sort(function (a, b) {
            if (b.end === a.end && b.start === a.start) {
                return a.type > b.type;
            }
            return b.start - a.start;
        });
        let result = '';
        tagToClose.forEach(function (decoratorToClose) {
            result += getDecoratorClosingTag(decoratorToClose);
        });
        return result;
    }

    function openInlineTag(section, offset) {
        let tagToClose = section.decorators.filter(function (decorator) {
            return offset === decorator.start;
        });
        tagToClose.sort(function (a, b) {
            if (b.end === a.end && b.start === a.start) {
                return a.type < b.type;
            }
            return b.end - a.end;
        });
        let result = '';
        tagToClose.forEach(function (decoratorToClose) {
            result += getDecoratorOpenningTag(decoratorToClose);
        });
        return result;
    }

    function createListTree(parentList, sectionsList) {
        let currentList = [];
        let section = sectionsList[0];
        changeList(section);

        while (sectionsList.length > 0 && (section.level > parentList.level || !parentList.level)) {
            section = sectionsList[0];
            if (section.level === currentList.level && section.type === currentList.type) {
                currentList.push(sectionsList.shift());
            } else if (section.level === currentList.level && section.type !== currentList.type) {
                changeList(section);
            } else if (section.level > currentList.level) {
                createListTree(currentList, sectionsList);
            } else if (section.level < currentList.level) {
                changeList(section);
            }
        }

        function changeList(currentSection) {
            currentList = [];
            currentList.level = currentSection.level;
            currentList.type = currentSection.type;
            parentList.push(currentList);
        }
    }

    function convertToHtml(listTree) {
        let result = '';
        if (listTree.length > 0) {
            let tag;
            if (listTree.type) {
                tag = getListTag(listTree.type);
                result += '<' + tag + '>';
            }
            listTree.forEach(function (item, index) {
                if (Array.isArray(item)) {
                    result += convertToHtml(item);
                } else {
                    if (index > 0) {
                        result += '</li>';
                    }

                    if (item.type && item.level) {
                        result += '<li' + ' class="' + item.type + ' ' + item.type + "_" + item.level + '">';
                        result += convertSectionContent(item);
                    } else {
                        result += '<li>' + item.content;
                    }
                }
            });
            if (listTree.type) {
                result += '</li>';
                result += '</' + tag + '>';
            }
        }

        return result;
    }

    HTMLExporter.convertListItem = function (sections) {
        let listTree = [];
        if (sections.length > 0) {
            createListTree(listTree, sections);
            return convertToHtml(listTree);
        } else {
            return '';
        }
    };

    HTMLExporter.exportSection = function (section) {
        if (section.type === 'attachment') {
            return '<p><a href="' + section.data + '" download>' + section.name + '</a></p>';
        }
        if (section.type === 'image') {
            return '<p><img src="' + section.data + '" /></p>';
        }
        let tag = getSectionTag(section);
        return '<' + tag + '>' + convertSectionContent(section) + '</' + tag + '>';
    };

    HTMLExporter.exportDocument = function (doc) {
        let result = '';
        let currentDefaultDiv = [];
        doc.children.forEach(function (child) {
            if (child.children /*is a div*/) {
                result += HTMLExporter.exportSectionList(currentDefaultDiv);
                currentDefaultDiv = [];
                result += HTMLExporter.exportDivision(child);
            } else {
                currentDefaultDiv.push(child);
            }
        });
        result += HTMLExporter.exportSectionList(currentDefaultDiv);
        return result;
    };

    HTMLExporter.exportSectionList = function (sectionList) {
        let result = '';
        let listSections = [];
        let previousSection;
        sectionList.forEach(function (section) {
            if (section.type === 'ulist' || section.type === 'olist') {
                listSections.push(section);
            } else {
                result += HTMLExporter.convertListItem(listSections);
                listSections = [];
                if (section.children) {
                    result += HTMLExporter.exportDivision(section);
                } else {
                    result += HTMLExporter.exportSection(section);
                }
            }
            previousSection = section;
        });
        if (listSections.length > 0) {
            result += HTMLExporter.convertListItem(listSections);
        }
        return result;
    };

    HTMLExporter.exportDivision = function (division) {
        let result = '';
        result += getDivisionOpenningTag(division);
        if (division.type === 'code') {
            division.children.forEach(function (section) {
                result += section.content + '\n';
            });
        } else {
            result += HTMLExporter.exportSectionList(division.children);
        }
        result += getDivisionClosingTag(division);
        return result;
    };

    module.exports = HTMLExporter;
})();
